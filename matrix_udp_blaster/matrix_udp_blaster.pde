import java.net.*;
import java.io.*;
import java.util.Arrays;

DatagramSocket socket;
DatagramPacket packet;

int matrixWidth = 96;
int matrixHeight = 38;

byte[] buffer = new byte[(matrixWidth/8)*matrixHeight]; //Set your buffer size as desired
Host host = new Host("10.0.0.42");
int udpPort = 6450;

void settings(){
    size(matrixWidth, matrixHeight);
}

void setup() {
    println("hello");
    frameRate(60);
}

void draw() {
    background(0,0,0);
    stroke(255,0,0);
    strokeWeight(sin(millis()*.008)*20+20);
    point(mouseX, mouseY);

    translate(width/2, height/2);
    rotate(sin(millis()*.001)*PI);
    strokeWeight(4);
    line(-100,0,100,0);

    loadPixels();
    for(int i = 0; i < (width/8)*height; i++){
        buffer[i] = 0;
    }
    for(int i = 0; i < width*height; i+=8){
        for(int j = 0; j < 8; j++){
            if(getRed(pixels[i+j]) != 0){
                buffer[i/8] |= (1 << (j));
                pixels[i+j] = color(0,255,0);
            }
            else {
                buffer[i/8] &= ~(1 << (j));
            }
        }
    }
    updatePixels();
    sendUDP(host, buffer);
}

int getRed(color c) {
    return (int)((c >> 16) & 0xFF);
}

public void sendUDP( Host _host, byte[] _data) {
    if(_data == null) return;
    DatagramPacket packet = new DatagramPacket(
            _data,
            _data.length,
            _host.address,
            udpPort
    );
    try {
        _host.dsocket.send(packet);
    }
    catch(Exception e) {
        println("failed to send");
        _host.connect();
    }
}

// host class used by artnet sender to define hosts
class Host{
    String ip;
    InetAddress address;
    DatagramSocket dsocket;
    Host(String _ip){
        ip = _ip;
    }
    void connect(){
            try {
                address = InetAddress.getByName(ip);
                dsocket = new DatagramSocket();
                println("artnet host connect "+ip);
            }
            catch(Exception e) {
                println("artnet could not connect");
                exit();
            }
    }
}
