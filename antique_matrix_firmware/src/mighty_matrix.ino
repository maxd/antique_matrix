
// https://gist.github.com/liftoff/f9fbb9ce79f4a6687ebf
// #include "MatrixMeister.h"
#include "PinMap.h"
#include "TeensyID.h"
#include "Ethernet3.h"


#define WIDTH 96
#define WIDTH_BYTES WIDTH/8
#define HEIGHT 38

#define USE_DHCP 0
#define UDP_PORT 6450
IPAddress ipAddress = {10,0,0,42};
EthernetUDP udpSocket;
#define FRAME_SIZE (WIDTH_BYTES)*HEIGHT
uint8_t packetBuffer[FRAME_SIZE];
uint8_t frameBuffer[FRAME_SIZE];
IntervalTimer rowTimer;
//                     A2 A1 A0
int rowTable[8][3] = {
                        {0,0,0},
                        {0,0,1},
                        {0,1,0},
                        {0,1,1},
                        {1,0,0},
                        {1,0,1},
                        {1,1,0},
                        {1,1,1}
                    };


void setup(){
    delay(1000);
    // while(!Serial);
    Serial.begin(9600);
    // start network before setting up matrix or else problems.
    networkInit();
    matrixInit();
    // draw a row every 420us
    rowTimer.begin(updateRow, 420);
}

void matrixInit(){
    pinMode(CLK_PIN, OUTPUT);
    pinMode(LAT_PIN, OUTPUT);
    pinMode(DATA_TOP_PIN, OUTPUT);
    pinMode(DATA_BOTTOM_PIN, OUTPUT);

    pinMode(OE_PIN, OUTPUT);
    // pinMode(QA, OUTPUT);
    pinMode(A0_PIN, OUTPUT);
    pinMode(A1_PIN, OUTPUT);
    pinMode(A2_PIN, OUTPUT);
    pinMode(E2_PIN, OUTPUT);
    pinMode(E3_PIN, OUTPUT);
    pinMode(E1A_PIN, OUTPUT);
    pinMode(E1B_PIN, OUTPUT);
    pinMode(E1C_PIN, OUTPUT);
    // pinMode(QB, OUTPUT);
    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(OE_PIN, HIGH);
}

void networkInit(){
    uint8_t mac[6];
    // gets teensy mac address
    teensyMAC(mac);
    pinMode(WIZNET_RESET_PIN, OUTPUT);
    digitalWrite(WIZNET_RESET_PIN, LOW);    // begin reset the WIZ820io
    //
    pinMode(WIZNET_CS_PIN, OUTPUT);
    digitalWrite(WIZNET_CS_PIN, HIGH);  // de-select WIZ820io
    //
    pinMode(SDCARD_CS_PIN, OUTPUT);
    digitalWrite(SDCARD_CS_PIN, HIGH);   // de-select the SD Card
    digitalWrite(WIZNET_RESET_PIN, HIGH);
    digitalWrite(WIZNET_CS_PIN, LOW);

    // with Ethernet.h init takes a optionnal CS pin
    // Ethernet.init(WIZNET_CS_PIN);
    // But with Ethernet3.h init takes the number of sockets.
    // either 1 (16k) 2 (8k) 4 (4k) else 8 (2k)
    Ethernet.init(4);
    Ethernet.setCsPin(WIZNET_CS_PIN);
    Ethernet.begin(mac, ipAddress);
    // if(USE_DHCP) {
    //     if(Ethernet.begin(mac)) { //}, 30000, 4000)){
    //         // for(int i = 0; i < 4; i++) {
    //         //     ipAddress[i] = Ethernet.localIP()[i];
    //         // }
    //         // DHCP anim?
    //     }
    // }
    // else {
    //     Ethernet.begin(mac, ipAddress);
    // }
    udpSocket.begin(UDP_PORT);

}


void loop(){
    int s = udpSocket.parsePacket();
    if(s >= FRAME_SIZE){
        udpSocket.read(packetBuffer, FRAME_SIZE);
        noInterrupts();
        memcpy(frameBuffer, packetBuffer, sizeof(frameBuffer));
        interrupts();
    }
}

// void setXY(int x, int y, int v){
//     bitWrite(frameBuffer[x/8 + y*WIDTH_BYTES], x%8, v);
// }

void updateRow(){
    static int row;
    row++;
    row%=HEIGHT/2;
    for(int j = 0; j < WIDTH_BYTES; j++){
        dualShit(DATA_TOP_PIN,
                DATA_BOTTOM_PIN,
                CLK_PIN,
                LSBFIRST,
                frameBuffer[(row*WIDTH_BYTES)+j],
                frameBuffer[(row+HEIGHT/2)*WIDTH_BYTES+j]
            );
    }
    set_row(row);
    digitalWrite(LAT_PIN, HIGH);
    digitalWrite(LAT_PIN, LOW);
}

void set_row(int i){
    digitalWrite(E2_PIN, LOW); // active low?
    digitalWrite(E3_PIN, HIGH); // active low?
    // chip select
    if(i < 8){
        digitalWrite(E1A_PIN, LOW);
        digitalWrite(E1B_PIN, HIGH);
        digitalWrite(E1C_PIN, HIGH);
    }
    else if(i < 16){
        digitalWrite(E1A_PIN, HIGH);
        digitalWrite(E1B_PIN, LOW);
        digitalWrite(E1C_PIN, HIGH);
    }
    else {
        digitalWrite(E1A_PIN, HIGH);
        digitalWrite(E1B_PIN, HIGH);
        digitalWrite(E1C_PIN, LOW);
    }
    digitalWrite(A2_PIN, rowTable[i%8][0]);
    digitalWrite(A1_PIN, rowTable[i%8][1]);
    digitalWrite(A0_PIN, rowTable[i%8][2]);
}


// control two shift register sharing clk/lat/en pins
void dualShit(
    uint8_t dataPinA,
    uint8_t dataPinB,
    uint8_t clockPin,
    uint8_t bitOrder,
    uint8_t valA,
    uint8_t valB)
{
	uint8_t i;
	for (i = 0; i < 8; i++)  {
		if (bitOrder == LSBFIRST){
            digitalWrite(dataPinA, !!(valA & (1 << i)));
            digitalWrite(dataPinB, !!(valB & (1 << i)));
        }
		else {
            digitalWrite(dataPinA, !!(valA & (1 << (7 - i))));
            digitalWrite(dataPinB, !!(valB & (1 << (7 - i))));
        }
	    delayMicroseconds(1);
		digitalWrite(clockPin, HIGH);
        delayMicroseconds(1);
		digitalWrite(clockPin, LOW);
	}
}
